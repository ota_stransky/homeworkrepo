package com.tieto.stranskyota.pin;


public interface PinNumbers {
	
	/**
	 * Checks if input number is valid as Norway personal identification number
	 * @param source is input <i>number</i>
	 * @return <i>true/false</i>
	 */
	public boolean isNorwayPin(String source);
	
	/**
	 * Checks if input number is valid as Czech personal identification number
	 * @param source is input <i>number</i>
	 * @return <i>true/false</i>
	 */
	public boolean isCzechPin(String source);

}
