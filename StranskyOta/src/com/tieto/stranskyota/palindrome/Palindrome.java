package com.tieto.stranskyota.palindrome;

import java.util.ArrayList;

public interface Palindrome {
	
	//---1st method---//
	/**
	 * Checks if the sentence is a palindrome
	 * @param s	for <i>String</i>
	 * @return <i>true/false</i>
	 */
	public boolean isPalindrome(String s);
	
	//---2nd method---//
	/**
	 * Get collection of sentences and return count of palindromes
	 * @param palCol collection of <i>sentences</i>
	 * @return <b>count</b> <i>palindromes</i>
	 */
	public int totalPalindromes(ArrayList<String> palCol);
	
	//---3rd method---//
	/**
	 * Get collection of sentences and return collection of palindromes
	 * @param palCol collection of <i>sentences</i>
	 * @return <b>newCol</b> collection of <i>palindromes</i>
	 */
	public ArrayList<String> getPalindromes(ArrayList<String> palCol);
}
