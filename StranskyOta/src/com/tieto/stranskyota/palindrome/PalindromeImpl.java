package com.tieto.stranskyota.palindrome;

import java.util.ArrayList;
import java.text.Normalizer;
import java.text.Normalizer.Form;

/**
 * Palindrome program
 * @author Str�nsk� Ota
 *
 */
public class PalindromeImpl implements Palindrome {


	@Override
	public boolean isPalindrome(String s) {

		String revWord = "";
		String origWord;

		if (s=="" || s==null || s.length()<2)  {
			return false;
			}
		origWord = s.toLowerCase();
		
		/* remove spaces and diacritics from string */
		origWord = Normalizer.normalize(origWord,Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
		origWord = origWord.replaceAll("[^\\p{ASCII}[^\\p{L}\\p{Z}[ ]]]", "");

		for (int i= origWord.length()-1; i >= 0; i--){
			revWord += origWord.charAt(i);}
		if (revWord.equals(origWord)){	
			return true;
			}
		else 
			return false;
	}

	@Override
	public int totalPalindromes(ArrayList<String> wordsCol) {
		int count = 0;

		for (String item : wordsCol)
			if (isPalindrome(item)) count++;
		return count;
	}

	@Override
	public ArrayList<String> getPalindromes(ArrayList<String> origCol) {

		ArrayList<String> newCol = new ArrayList<String>();
		
		for (String item : origCol) {
			if (isPalindrome(item))
				newCol.add(item);
		}
		return newCol;
	}

}
