package com.tieto.stranskyota;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.tieto.stranskyota.pin.PinNumbersImpl;

public class NorwayPINTest {
	
	private PinNumbersImpl pin;

	@Before
	public void setUp() throws Exception {
		pin = new PinNumbersImpl();
	}
	
	@Test
	public void isNorwayPin1() {
		String number = "21075013849";
		assertTrue(pin.isNorwayPin(number));
	}
	
	@Test
	public void isNorwayPin2() {
		String number = "24072127108";
		assertFalse(pin.isNorwayPin(number));
	}
	
	@Test
	public void isNorwayPin3() {
		String number = "17109208930";
		assertTrue(pin.isNorwayPin(number));
	}
	
	@Test
	public void isNorwayPin4() {
		String number = "25520055464";
		assertFalse(pin.isNorwayPin(number));
	}
	
	@Test
	public void isNorwayPin5() {
		String number = "14051466927";
		assertTrue(pin.isNorwayPin(number));
	}

}
