package com.tieto.stranskyota;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.tieto.stranskyota.palindrome.Palindrome;
import com.tieto.stranskyota.palindrome.PalindromeImpl;

public class CountPalindromesTest {

	Palindrome pal;
	ArrayList<String> words = new ArrayList<String>();
	
	@Before
	public void setUp() throws Exception {
		pal = new PalindromeImpl();
	}
	
	@Test
	public void countOfPalindromes() {
		ArrayList<String> testcount = new ArrayList<String>();
		testcount.add("anna");
		testcount.add("albert");
		testcount.add("M�slo");
		testcount.add("RADAR");
		testcount.add("Ah�");
		
		int count = pal.totalPalindromes(testcount);
		assertEquals(3, count);
	}
}