package com.tieto.stranskyota;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.tieto.stranskyota.palindrome.Palindrome;
import com.tieto.stranskyota.palindrome.PalindromeImpl;

public class GetPalindromesTest {

	Palindrome pal;
	ArrayList<String> words = new ArrayList<String>();
	
	@Before
	public void setUp() throws Exception {
		pal = new PalindromeImpl();
	}
	
	@Test
	public void newPalindromesCollection() {
		words.add("anna");
		words.add("albert");
		words.add("M�slo");
		words.add("RADAR");
		words.add("Ah�");
		
		ArrayList<String> collection = pal.getPalindromes(words);
		ArrayList<String> testingCollection = new ArrayList<String>();
		testingCollection.add("anna");
		testingCollection.add("RADAR");
		testingCollection.add("Ah�");
		assertEquals(collection, testingCollection);
	}	
}