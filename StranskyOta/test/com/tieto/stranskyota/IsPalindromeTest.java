package com.tieto.stranskyota;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.tieto.stranskyota.palindrome.Palindrome;
import com.tieto.stranskyota.palindrome.PalindromeImpl;

public class IsPalindromeTest {

	private Palindrome pal;
	
	@Before
	public void setUp() throws Exception {
		pal = new PalindromeImpl();
	}
	
	@Test
	public void isPalindrome() {
		assertTrue(pal.isPalindrome("anna"));
	}
	
	@Test
	public void isNotPalindrome() {
	assertFalse(pal.isPalindrome("anička"));
	}
	
	@Test
	public void oneCharacterIsNotPalindrome() {
		assertFalse(pal.isPalindrome("a"));
	}
	
	@Test
	public void emptyStringIsNotPalindrome() {
		assertFalse(pal.isPalindrome(""));
	}
	
	@Test
	public void nullIsNotPalindrome() {
		assertFalse(pal.isPalindrome(null));
	}
	
	@Test
	public void palindromeWithSpaces() {
		assertTrue(pal.isPalindrome("kobyla ma maly bok"));
	}
	
	@Test
	public void mixedCaseIsPalindrome() {
		assertTrue(pal.isPalindrome("KobYlaMAmalyboK"));
	}
	
	@Test
	public void palindromeWithDiacritics() {
		assertTrue(pal.isPalindrome("Kobylamámalýbok"));
	}	
}
