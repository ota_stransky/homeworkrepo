package com.tieto.stranskyota;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.tieto.stranskyota.pin.PinNumbersImpl;

public class CzPINTest {
	
	private PinNumbersImpl pin;

	@Before
	public void setUp() throws Exception {
		pin = new PinNumbersImpl();
	}
	
	@Test
	public void isCzechPIN1() {
		assertTrue(pin.isCzechPin("9602152362"));
	}
	
	@Test
	public void isCzechPIN2() {
		assertFalse(pin.isCzechPin("6951135459"));
	}
	
	@Test
	public void isCzechPIN3() {
		assertTrue(pin.isCzechPin("6452020520"));
	}
	
	@Test
	public void isCzechPIN4() {
		assertFalse(pin.isCzechPin("310431873"));
	}
	
	@Test
	public void isCzechPIN5() {
		assertTrue(pin.isCzechPin("6703121986"));
	}
}
