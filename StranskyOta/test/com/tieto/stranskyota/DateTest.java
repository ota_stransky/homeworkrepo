package com.tieto.stranskyota;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.tieto.stranskyota.pin.PinNumbersImpl;

public class DateTest {
	
	private PinNumbersImpl pin;

	@Before
	public void setUp() throws Exception {
		pin = new PinNumbersImpl();
	}
	
	@Test
	public void isDate1() {
		assertTrue(pin.isDateOK(29, 2, 04));
	}
	
	@Test
	public void isDate2() {
		assertFalse(pin.isDateOK(31, 4, 77));
	}
	
	@Test
	public void isDate3() {
		assertFalse(pin.isDateOK(30, 2, 89));
	}
	
	@Test
	public void isDate4() {
		assertTrue(pin.isDateOK(14, 3, 54));
	}
}
